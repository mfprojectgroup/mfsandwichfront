var sandwichComponents = [];
var totalKcal = 0;
var totalPrice = 0;
var sandwichComponentLayerNumber = 0;


function getComponents() {
  fetch('http://localhost:8080/components')

    .then(response => response.json()) // funktsioon, mis võtab parameetrina vastu teise funktsiooni, et seda vastavalt vajadusele käivitada.
    //.then (prepareDisplayCompanies);
    .then(components => displayComponents(components));
}

function displayComponents(components) {
  let componentList = document.getElementById('componentList');
  componentList.innerHTML = "";

  for (let i = 0; i < components.length; i++) {
    componentList.innerHTML += `
    <ul class="main-buttons">
         <li>
           <i class="fa fa-circle fa-2x"></i>
           <button class="main-buttons" onClick="addIngredient(${components[i].id}, '${components[i].name}',
            '${components[i].component_art}','${components[i].picture}', ${components[i].unitPrice},
             ${components[i].unitKcal})">${components[i].name}</button>
           <ul class="hidden">
             <li><img src=${components[i].picture} style="width:100%"></li>
             <li>hind ${components[i].unitPrice} €</li>
             <li>kCal ${components[i].unitKcal}</li>
           </ul>
        </li>

  `;
  }

}

function addIngredient(id, name, component_art, picture, unitPrice, unitKcal) {
  let selectedComponent = {
    id,
    name,
    component_art,
    picture,
    unitPrice,
    unitKcal
  };

  sandwichComponents.push(selectedComponent);

  totalKcal += unitKcal;
  totalPrice += unitPrice;

  document.getElementById("Kalorid").innerHTML = totalKcal.toFixed(0) + " kCal";
  document.getElementById("Hind").innerHTML = totalPrice.toFixed(2) + " €";

  const sandwichComponentsDiv = document.getElementById('sandwichComponentsDiv');
  let componentDiv = document.createElement("div");
  componentDiv.id = `div${sandwichComponentLayerNumber}`;
  componentDiv.classList.add(`${component_art}`);
  componentDiv.style.bottom = `${sandwichComponentLayerNumber * 20}px`;
  componentDiv.addEventListener("click", deleteComponent);
  sandwichComponentsDiv.appendChild(componentDiv);
  sandwichComponentLayerNumber++;
}

function deleteComponent() {
  if (sandwichComponents != null && sandwichComponents.length > 0) {
    totalKcal = totalKcal - sandwichComponents[sandwichComponents.length - 1].unitKcal;
    document.getElementById("Kalorid").innerHTML = totalKcal.toFixed(0) + " kCal";
    totalPrice = totalPrice - sandwichComponents[sandwichComponents.length - 1].unitPrice;
    document.getElementById("Hind").innerHTML = totalPrice.toFixed(2) + " €";
  }

  let sandwichComponentDiv = document.getElementById(`div${sandwichComponentLayerNumber-1}`);
  sandwichComponentDiv.parentNode.removeChild(sandwichComponentDiv);
  sandwichComponents.pop();
  sandwichComponentLayerNumber--;
}

function deleteAll() {
  while (sandwichComponents.length) {
    totalKcal = totalKcal - sandwichComponents[sandwichComponents.length - 1].unitKcal;
    document.getElementById("Kalorid").innerHTML = totalKcal.toFixed(0) + " kCal";
    totalPrice = totalPrice - sandwichComponents[sandwichComponents.length - 1].unitPrice;
    document.getElementById("Hind").innerHTML = totalPrice.toFixed(2) + " €";
    let sandwichComponentDiv = document.getElementById(`div${sandwichComponentLayerNumber-1}`);
    sandwichComponentDiv.parentNode.removeChild(sandwichComponentDiv);
    sandwichComponents.pop();
    sandwichComponentLayerNumber--;

  }
}